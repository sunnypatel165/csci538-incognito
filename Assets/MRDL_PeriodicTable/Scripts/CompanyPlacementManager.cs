﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.MRDL.PeriodicTable;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;

public class CompanyPlacementManager : MonoBehaviour
{
    
    public GridObjectCollection companyDataCollection;
    public Transform companyPlacementManagerTransform;
    
    public void ChangeLayoutStylePlane()
    {
        if(companyDataCollection != null)
        {
            PeriodicTableLoader ptl = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
            ptl.toggleClusterLabels(false);
            ptl.toggleNetworkEdges(false);
            
            companyDataCollection.SurfaceType = ObjectOrientationSurfaceType.Plane;
            companyDataCollection.OrientType = OrientationType.FaceParentFoward;
            companyDataCollection.Radius = 0.5f;
            companyDataCollection.RadialRange = 180.0f;
            companyDataCollection.Rows = 6;
            companyDataCollection.CellWidth = 0.5f;
            companyDataCollection.CellHeight = 0.5f;
            companyDataCollection.UpdateCollection();

            companyPlacementManagerTransform.localPosition = new Vector3(0.0f, 0.1f, 1.5f);
            
            ptl.setLayoutStyle("planar");
        }
    }

    public void ChangeLayoutStyleCylinder()
    {
        if (companyDataCollection != null)
        {
            PeriodicTableLoader ptl = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
            ptl.toggleClusterLabels(false);
            ptl.toggleNetworkEdges(false);
            
            companyDataCollection.SurfaceType = ObjectOrientationSurfaceType.Cylinder;
            companyDataCollection.OrientType = OrientationType.FaceOrigin;
            companyDataCollection.Radius = 01.2f;
            companyDataCollection.RadialRange = 180.0f;
            companyDataCollection.Rows = 4;
            companyDataCollection.CellWidth = 0.45f;
            companyDataCollection.CellHeight = 0.45f;
            companyDataCollection.UpdateCollection();

            companyPlacementManagerTransform.localPosition = new Vector3(0.0f, 0.15f, 1.5f);
            
            ptl.setLayoutStyle("cylinder");
        }
    }

    public void ChangeLayoutStyleRadial()
    {
        if (companyDataCollection != null)
        {
            PeriodicTableLoader ptl = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
            ptl.toggleClusterLabels(false);
            ptl.toggleNetworkEdges(false);
            
            companyDataCollection.SurfaceType = ObjectOrientationSurfaceType.Radial;
            companyDataCollection.OrientType = OrientationType.FaceCenterAxis;
            companyDataCollection.Radius = 12.0f;
            companyDataCollection.RadialRange = 90.0f;
            companyDataCollection.Rows = 6;
            companyDataCollection.CellWidth = 1.0f;
            companyDataCollection.CellHeight = 1.0f;
            companyDataCollection.UpdateCollection();

            companyPlacementManagerTransform.localPosition = new Vector3(0.0f, 0f, 1.5f);
            
            ptl.setLayoutStyle("radial");
        }
    }

    public void ChangeLayoutStyleSphere()
    {
        if (companyDataCollection != null)
        {
            PeriodicTableLoader ptl = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
            ptl.toggleClusterLabels(false);
            ptl.toggleNetworkEdges(false);
            
            companyDataCollection.SurfaceType = ObjectOrientationSurfaceType.Sphere;
            companyDataCollection.OrientType = OrientationType.FaceOrigin;
            companyDataCollection.Radius = 1.2f;
            companyDataCollection.RadialRange = 180.0f;
            companyDataCollection.Rows = 5;
            companyDataCollection.CellWidth = 0.8f;
            companyDataCollection.CellHeight = 0.5f;
            companyDataCollection.UpdateCollection();

            companyPlacementManagerTransform.localPosition = new Vector3(0.0f, -0.33f, 1.5f);

            ptl.setLayoutStyle("sphere");
        }
    }


    public void ResetLayoutStyle()
    {
        if (companyDataCollection != null)
        {
            PeriodicTableLoader ptl = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
            
            companyPlacementManagerTransform.localPosition = new Vector3(0.0f, -0.7f, 1f);
            companyPlacementManagerTransform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
            
            ptl.resetLayoutStyle();
        }
    }
    
}
