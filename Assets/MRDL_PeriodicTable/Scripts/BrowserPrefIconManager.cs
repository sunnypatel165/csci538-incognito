﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BrowserPrefIconManager : MonoBehaviour
{
    public Button cameraPerm_Btn;        
    public Button adsPerm_Btn;        
    public Button flashPerm_Btn;        
    public Button javascriptPerm_Btn;        
    public Button locationPerm_Btn;        
    public Button microphonePerm_Btn;        
    public Button notificationsPerm_Btn;        
    public Button paymentPerm_Btn;        
    public Button soundPerm_Btn;        
    public Button usbPerm_Btn; 
    
    // Start is called before the first frame update
    void Start()
    {
        cameraPerm_Btn.onClick.AddListener(() => PrefBtnHandler(cameraPerm_Btn));
        adsPerm_Btn.onClick.AddListener(() => PrefBtnHandler(adsPerm_Btn));
        flashPerm_Btn.onClick.AddListener(() => PrefBtnHandler(flashPerm_Btn));
        javascriptPerm_Btn.onClick.AddListener(() => PrefBtnHandler(javascriptPerm_Btn));
        locationPerm_Btn.onClick.AddListener(() => PrefBtnHandler(locationPerm_Btn));
        microphonePerm_Btn.onClick.AddListener(() => PrefBtnHandler(microphonePerm_Btn));
        notificationsPerm_Btn.onClick.AddListener(() => PrefBtnHandler(notificationsPerm_Btn));
        paymentPerm_Btn.onClick.AddListener(() => PrefBtnHandler(paymentPerm_Btn));
        soundPerm_Btn.onClick.AddListener(() => PrefBtnHandler(soundPerm_Btn));
        usbPerm_Btn.onClick.AddListener(() => PrefBtnHandler(usbPerm_Btn));
    }

    private void PrefBtnHandler(Button btn)
    {
        string name = btn.GetComponent<Image>().sprite.name;
        name = GetFlippedIconName(name);
        string icon_path = String.Concat("Icons/", name);
        Sprite new_icon_sprite = GetSpriteFromName(icon_path);
        btn.GetComponent<Image>().sprite = new_icon_sprite;
        btn.GetComponent<Image>().sprite.name = name;
    }

    //Function to repalce gs with color and vice-versa, for toggling the image. 
    public static String GetFlippedIconName(String name)
    {
        if (name.Contains("_gs"))
        {
            name = name.Replace("_gs", "_color");
        }
        else if (name.Contains("_color"))
        {
            name = name.Replace("_color", "_gs");
        }

        return name;
    }
    public static Sprite GetSpriteFromName(String icon_path)
    {

        Texture2D new_icon_texture = Resources.Load(icon_path) as Texture2D;
        Sprite new_icon_sprite = Sprite.Create(new_icon_texture, new Rect(0, 0, new_icon_texture.width,
            new_icon_texture.height), new Vector2(0.5f, 0.5f));
        return new_icon_sprite;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
}
