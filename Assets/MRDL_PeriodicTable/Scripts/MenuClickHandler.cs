﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.MRDL.PeriodicTable;
using System;

public class MenuClickHandler : MonoBehaviour
{
    //To detect already fortified or not
    private Boolean fortifiedAll = false;
    public void fortifyAll()
    {
        if (!fortifiedAll)
        {
            Debug.Log("Inside FortifyClickHandler");
            PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
            p.fortifyAll();
            fortifiedAll = true;
        }
        else
        {
            Debug.Log("Already FortifiedAll!");
        }
    }
    public void fortifyOne()
    {
        //This is actually not working because the references of the script are different for 2 buttons
        //But it is taken care by the map companyNameFortifiedMap in PeriodicTableLoader
        if (!fortifiedAll)
        {
            Debug.Log("individual");
            PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
            p.fortifyActiveCompany();
        }
        else
        {
            Debug.Log("Already FortifiedAll");
        }
    }
    public void refreshClick()
    {
        Debug.Log("Refresh clicked");
        PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
        p.loadData(true);
    }
    public void toggleGraph()
    {
        Debug.Log("Toggle Graph clicked");
        PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
        p.toggleEdges();
    }
    
    public void sortCompanies()
    {
        Debug.Log("Sort Companies clicked");
        PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
        p.toggleSortingOfCompanies();
    }
    public void showOnlyLabelsToggle()
    {
        Debug.Log("showOnlyLabelsToggle clicked");
        PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
        p.showOnlyLabelsToggle();
    }
}
