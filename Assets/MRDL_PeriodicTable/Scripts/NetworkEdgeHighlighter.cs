﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.MRDL.PeriodicTable;
using UnityEngine;

public class NetworkEdgeHighlighter : MonoBehaviour
{
    // Utility to highlight edges
    public void highlightEdges()
    {
        PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
        p.highlightEdges(this.transform.Find("CompanyName").GetComponent<TextMesh>().text.ToLower());
    }
    
    // Utility to reset edges
    public void resetHighlightedEdges()
    {
        PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
        p.resetHighlightedEdges(this.transform.Find("CompanyName").GetComponent<TextMesh>().text.ToLower());
    }
    
    // Utility to highlight edge on hover
    public void highlightIndividualEdge()
    {
        PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
        p.highlightIndividualEdge(this.gameObject);
    }
    
    // Utility to highlight edge on hover
    public void resetHighlightIndividualEdge()
    {
        PeriodicTableLoader p = (HoloToolkit.MRDL.PeriodicTable.PeriodicTableLoader)FindObjectOfType(typeof(PeriodicTableLoader));
        p.resetIndividualHighlightedEdge(this.gameObject);
    }
}
