﻿//
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace HoloToolkit.MRDL.PeriodicTable
{
    [System.Serializable]
    public class CompanyData
    {
        public string name;
        public int xpos;
        public int ypos;
        public string logo_loc;
        public string category;
        public string source;
        public float threat_score;
        public CookieStats cookie_statistics;
        public BrowserPerm[] browser_permissions;
        public Alternative[] alternatives;
        public Tracker[] trackers;
        public PastScandal[] past_scandals;
    }

    [System.Serializable]
    public class Alternative
    {
        public string name;
        public string logo_loc;
        public float threat_score;
    }

    [System.Serializable]
    public class Tracker
    {
        public string name;
        public string[] companies;
    }

    [System.Serializable]
    public class BrowserPerm
    {
        public string name;
        public string icon_loc;
    }

    [System.Serializable]
    public class CookieStats
    {
        public int user_cookies;
        public int session_cookies;
        public int thirdparty_cookies;
    }
    
    [System.Serializable]
    public class PastScandal
    {
        public string headline;
        public string source;
        public string source_title;
        public string author;
    }

    [System.Serializable]
    public class TrackerInfo
    {
        public CompanyData company;
        public Dictionary<string, string> data = new Dictionary<string, string>();
    }

    [System.Serializable]
    class CompaniesData
    {
        public List<CompanyData> companyData;

        public static CompaniesData FromJSON(string json)
        {
            return JsonUtility.FromJson<CompaniesData>(json);
        }
    }

    public class PeriodicTableLoader : MonoBehaviour
    {
        // Layout Manager
        public Transform CompanyLayoutManager;
        
        // GirdObjectManager
        public GridObjectCollection Collection;

        // Prefabs
        public GameObject CompanyPrefab;
        public GameObject CompanyPrefabDuck;
        public GameObject CompanyClusterPrefab;
        public GameObject NetworkEdgePrefab;

        // Spacing between every company cube prefab (Default : 0.3)
        public float boxSeparation;

        // Thickness of the network edge prefab (Default : 0.03)
        public float edgeRadius;
        // The scaling of the network edge prefab cylinder with respect to company cubes (CAREFUL before changing this)
        public float edgeScaleFactor = 0.5125f;
        
        // Offsets for cluster labels
        public float label_y_offset = 1.0f;        // -1.0f for within cluster
        // Cluster label orb base radius scaling
        public float label_orb_radius_scale = 0.14f;        // default value : 0.14
        
        // The extent all game objects are project ahead in the z -axis
        private static float z_push = 2.0f;

        // Materials for companies
        public Material[] catMats;
        
        // Flag to toggle highlight of connectors on hover, default is true
        public Boolean highlightEdgeOnFocus = true;
        // Materials for edge highlighting
        public Material default_edge_material;
        public Material highlight_edge_material;
        public Material individual_edge_material;
        
        // Data loading parameters
        public bool loadDataLocally;
        public string jsonDataUrl;

        private void Start()
        {
            loadData();
        }

        //Helper to load data.
        public void loadData(Boolean refreshed=false)
        {
            if (loadDataLocally && isLayoutReseted)
            {
                InitializeData(fetchLocalJsonData());
            }
            else
            {
                StartCoroutine(fetchRemoteJsonData(refreshed));
            }
        }
        
        // Set to hold types of categories encountered
        HashSet<string> categoriesSet = new HashSet<string>();

        //Dict to hold company name vs company data object
        Dictionary<String, CompanyData> companyNameObjMap = new Dictionary<String, CompanyData>();

        // Dict to hold company data object v/s company Gameobject
        Dictionary<CompanyData, GameObject> companyObjGameObjMap = new Dictionary<CompanyData, GameObject>();

        // Dict to hold company name v/s company Gameobject
        Dictionary<string, GameObject> companyNameGameObjMap = new Dictionary<string, GameObject>();
        
        // Dict to hold Gameobject v/s original position vectors
        Dictionary<GameObject, Vector3> companyGameObjOrgVectorMap = new Dictionary<GameObject, Vector3>();

        // Dict to hold company name v/s whether it is fortified or not
        Dictionary<string, Boolean> companyNameFortifiedMap = new Dictionary<string, Boolean>();
        
        // Dict to hold Category Name v/s Its LabelGameobject
        Dictionary<string, GameObject> catNameLabelGameObjMap = new Dictionary<string, GameObject>();
        
        // List to hold all network edge Gameobjects
        List<GameObject> CompEdgeGameObjList = new List<GameObject>();
        
        // Dict to hold company name v/s networkEdge game objects list
        Dictionary<string, List<GameObject>> compGameObjNetEdgeList = new Dictionary<string, List<GameObject>>();

        // Dict to hold company object v/s tracker info
        Dictionary<CompanyData, List<TrackerInfo>> companyNetwork = new Dictionary<CompanyData, List<TrackerInfo>>();

        // Variable to keep track of network edges
        Boolean isGraphActive = false;
        
        // Variable to keep track whether companies displayed are sorted on threat-score or not
        Boolean isSortingActive = false;

        // Variable to keep track whether only the labels are shown or not
        Boolean isLabelOnly = true;
        
        // Variable to keep track whether any special layout is used or its reset to plus pattern
        Boolean isLayoutReseted = true;
        
        // Dict to hold cluster label name v/s orb game objects
        private Dictionary<string, GameObject> ClusterLabelTransformMap = new Dictionary<string, GameObject>();
        
        // Dict to hold cluster label name v/s count of trackers in and out
        private Dictionary<string, int> ClusterLabelTrackerCount = new Dictionary<string, int>();
        
        // Dict to hold the category specific colors
        private Dictionary<string, Color> catColorDict = new Dictionary<string, Color>()
        {
            {"education", new Color32(161, 196, 180, 255)},
            {"finance", new Color32(191, 133, 191, 255)},
            {"news", new Color32(137, 122, 173, 255)},
            {"shopping", new Color32(126, 180, 206, 255)},
            {"entertainment", new Color32(111, 193, 78, 255)},
            {"housing", new Color32(144, 177, 161, 255)},
            {"social", new Color32(227, 135, 71, 255)},
            {"searchengine", new Color32(227, 98, 122, 255)},
            {"travel", new Color32(215, 197, 114, 255)},
            {"politics", new Color32(94, 180, 133, 255)},
            {"healthcare", new Color32(255, 255, 0, 255)},
            {"jobs", new Color32(0, 76, 106, 255)},
        };
        
        // Dict to hold cluster label name v/s position location
        private Dictionary<string, float[]> clusterLabelGameObjPositionMap = new Dictionary<string, float[]>()
        {
            {"shopping", new[]{0, -1, z_push}},
            {"entertainment", new[]{-4, 3, z_push}},
            {"social", new[]{4, 3, z_push}},
            {"searchengine", new[]{0, 7, z_push}},
            {"travel", new[]{0, 3, z_push}}
        };

        //Legend
        GameObject threatScoreLegend;
        
        // Method to refresh and reinitialize and scene game objects by looking only at what changed and not touching
        // the existing game objects 
        public void refreshReInitialize(string data)
        {
            // Load and parse the data
            List<CompanyData> companies = CompaniesData.FromJSON(data).companyData;
            
            // Get new list of companies
            List<string> newCompNames = companies.Select(comp => comp.name.ToLower()).ToList();
            
            // Get existing list of companies
            List<string> oldCompNames = new List<string>(companyNameGameObjMap.Keys);
            
            // Get companies added
            List<string> compsAdded = newCompNames.Except(oldCompNames).ToList();
            List<CompanyData> compsAddedData = companies.Where(comp => compsAdded.Contains(comp.name.ToLower())).ToList();
            
            // Get companies removed
            List<string> compsRemoved = oldCompNames.Except(newCompNames).ToList();
            
            // Process newly added companies
            processData(compsAddedData, true);
            
            // Process removed companies
            
        }
        
        // Init the whole scene and all game objects at the start
        public void InitializeData(string data)
        {
            // Load and parse the data
            List<CompanyData> companies = CompaniesData.FromJSON(data).companyData;
            
            // Process the loaded companies
            processData(companies);
        }

        // Process data for each company
        private void processData(List<CompanyData> companies, Boolean refreshed=false)
        {
            // Dict to hold Category name v/s their respective material 
            Dictionary<string, Material> catMatDict = new Dictionary<string, Material>()
            {
                {"education", catMats[0]},
                {"finance", catMats[1]},
                {"news", catMats[2]},
                {"shopping", catMats[3]},
                {"entertainment", catMats[4]},
                {"housing", catMats[5]},
                {"social", catMats[6]},
                {"searchengine", catMats[7]},
                {"travel", catMats[8]},
                {"politics", catMats[9]},
                {"healthcare", catMats[10]},
                {"jobs", catMats[11]},
            };

            // Insantiate the element prefabs in their correct locations and with correct text
            foreach (CompanyData company in companies)
            {
                GameObject companyGameObject;
                Vector3 originalPosVector;
                if (company.name.ToLower().Equals("duckduckgo"))
                {
                    companyGameObject = Instantiate<GameObject>(CompanyPrefabDuck, CompanyLayoutManager);
                    companyGameObject.GetComponentInChildren<Company>().SetFromCompanyData(company, catMatDict);
                    originalPosVector = new Vector3(company.xpos * boxSeparation, company.ypos * boxSeparation, z_push);
                    companyGameObject.SetActive(false);
                }
                else
                {
                    companyGameObject = Instantiate<GameObject>(CompanyPrefab, CompanyLayoutManager);
                    companyGameObject.GetComponentInChildren<Company>().SetFromCompanyData(company, catMatDict);
                    originalPosVector = new Vector3(company.xpos * boxSeparation, company.ypos * boxSeparation, z_push);
                    companyGameObject.SetActive(false);
                }

                companyNameGameObjMap[company.name.ToLower()] = companyGameObject;
                companyObjGameObjMap[company] = companyGameObject;
                companyNameObjMap[company.name.ToLower()] = company;
                companyGameObjOrgVectorMap[companyGameObject] = originalPosVector;

                if (!categoriesSet.Contains(company.category.ToLower()))
                {
                    // populate the types of categories encountered
                    categoriesSet.Add(company.category.ToLower());

                    // init category trackers count
                    ClusterLabelTrackerCount[company.category.ToLower()] = 0;   
                }

                // init game obj v/s network edges map
                compGameObjNetEdgeList[company.name.ToLower()] = new List<GameObject>();
                
                // Handle separately is refreshed state
                if (refreshed)
                {
                    companyGameObject.transform.localPosition = originalPosVector;
                    
                    // Set cube visibility based off on drill down state
                    companyGameObject.SetActive(!isLabelOnly);
                }
            }

            // Position company cubes based on the original positions of the JSON
            //THIS LINE IS COMMENTED TO ENABLE DEFAULT VIEW TO BE LABELS ONLY
            //PositionCompanyCubes(companyGameObjOrgVectorMap);

            // Populate the network adjacency list data-structure
            Dictionary<CompanyData,List<TrackerInfo>> localCN = populateNetworkAdjacencyList(companies);

            // Cluster labeling logic
            generateClusterLabels();

            //Find the location of the fortify button
            populateFortifyButtonPosition();
            
            // Check if called triggered when initial init or in refresh
            if (!refreshed)
            {
                // Make Cluster Labels visible
                toggleClusterLabels(true);
                
                // Place the threat score legend
                threatScoreLegend = GameObject.Find("ThreatScoreLegend");

                //THIS LEGEND IS HIDDEN, TO ENABLE DEFAULT VIEW TO BE LABELS ONLY
                threatScoreLegend.SetActive(false);   
            }
//            else
//            {
//                // Make Cluster Labels invisible
//                toggleClusterLabels(false);
//                
//                // Generate edges game objects
//                constructNetworkEdges(localCN);
//            }
        }

        // Utility to generate all cluster labels and its respective orbs
        private void generateClusterLabels()
        {
            foreach (var cat in categoriesSet)
            {
                if (!catNameLabelGameObjMap.ContainsKey(cat))
                {
                    GameObject clusterLabelObj = Instantiate<GameObject>(CompanyClusterPrefab, CompanyLayoutManager);
                    clusterLabelObj.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = cat;
                    clusterLabelObj.transform.localPosition = new Vector3(clusterLabelGameObjPositionMap[cat][0] * boxSeparation,
                        clusterLabelGameObjPositionMap[cat][1] * boxSeparation, clusterLabelGameObjPositionMap[cat][2]);

                    catNameLabelGameObjMap[cat] = clusterLabelObj;

                    // Color the label orb
                    Transform ClusterLabelCirclePS = clusterLabelObj.transform.Find("ClusterLabelOrb").Find("Circle");
                    ParticleSystem.MainModule circlePSMA = ClusterLabelCirclePS.GetComponent<ParticleSystem>().main;
                    circlePSMA.startColor = catColorDict[cat.ToLower()];

                    // Scale the label orbs
                    float trackerCountScale = ClusterLabelTrackerCount[cat.ToLower()] / 100.0f;
                    ClusterLabelCirclePS.localScale = new Vector3(label_orb_radius_scale + trackerCountScale,
                        label_orb_radius_scale + trackerCountScale, label_orb_radius_scale + trackerCountScale);

                    // Populate the Cluster label orbs transforms
                    GameObject clusterLabelOrbGameObj = new List<GameObject>(GameObject.FindGameObjectsWithTag("ClusterLabelOrb"))
                        .Find(g => g.transform.IsChildOf(clusterLabelObj.transform));
                    ClusterLabelTransformMap[cat.ToLower()] = clusterLabelOrbGameObj;
                }
            }
        }

        //Fortify all companies
        public void fortifyAll()
        {
            //For every company
            foreach (var company in companyNameGameObjMap)
            {
                //if not already fortified
                if (!companyNameFortifiedMap.ContainsKey(company.Key))
                {
                    //Update the threatScore for the company
                    updateThreatScoreForACompany(company.Key);

                    //Randomly toggle the permissions for a company
                    randomlyTogglePermissionImagesFor(company.Key);

                    //Fortify it!
                    companyNameFortifiedMap[company.Key] = true;
                }
                else
                {
                    Debug.Log("Already fortified, so skipping " + company.Key);
                }
            }
        }
        
        String[] orbNames = new string[] { "BlueOrb", "GreenOrb", "YellowOrb", "OrangeOrb", "RedOrb" };
        public void changeOrb(String company, float newScore)
        {
            Transform[] allChildren = companyNameGameObjMap[company].GetComponentsInChildren<Transform>();
            foreach (var child in allChildren)
            {
                if (child.name.Equals("ThreatScore"))
                {
                    Transform[] children = child.GetComponentsInChildren<Transform>(true);
                    foreach(var c in children)
                    {
                        if (c.name.Contains("Orb"))
                        {
                            if(c.name.Equals(orbNames[(int)(newScore / 20)]))
                            {
                                c.gameObject.SetActive(true);
                            }
                            else
                            {
                                c.gameObject.SetActive(false);
                            }
                        }
                    }
                }
            }

        }

        //Update threat score of a company
        public void updateThreatScoreForACompany(String company)
        {
            TextMesh[] children = companyNameGameObjMap[company].GetComponentsInChildren<TextMesh>();

            //Update the child which is ThreatScore
            foreach (var child in children)
            {
                if (Equals(child.name, "ThreatScoreTextOnly"))
                {
                    //Reduce the score by a RandomNumber between 0 and 15
                    float score = float.Parse(child.text.Trim());
                    float reduce = UnityEngine.Random.Range(5.0f, 15.0f);
                    float newScore = (Math.Max(0, (int)(score - reduce)));
                    child.text = newScore.ToString();

                    // Threat score color
                    child.color = Company.GetThreatScoreColor(newScore, false);

                    companyNameObjMap[company.ToLower()].threat_score = newScore;

                    // Change orb color
                    changeOrb(company, newScore);
                }
            }
        }

        //Randomly toggle permissions of a company
        public void randomlyTogglePermissionImagesFor(String companyName)
        {
            //Get all children buttons. Pass "true" otherwise inactive buttons will not be returned and a lot of them will be inactive
            RectTransform[] buttons = companyNameGameObjMap[companyName].GetComponentsInChildren<RectTransform>(true);

            //For every button 
            foreach (var button in buttons)
            {
                //Check if it is the perm button
                if (button.name.Contains("_Btn") && !button.name.Equals("PastScandals_Btn"))
                {
                    //Find the images
                    Image[] buttonImages = button.GetComponentsInChildren<Image>();

                    //For every image
                    foreach (var buttonImage in buttonImages)
                    {
                        //If the name contains color
                        if (buttonImage.sprite.name.Contains("_color"))
                        {
                            //Flip a coin and decide whether to flip the icon or not
                            var flip = UnityEngine.Random.Range(0, 2);
                            if (flip == 1)
                            {
                                //Find flipped name
                                var name = BrowserPrefIconManager.GetFlippedIconName(buttonImage.sprite.name);
                                Debug.Log("Chose to flip " + buttonImage.sprite.name + " to " + name + " for " + companyName);

                                //update the icon and prepare sprite
                                Sprite icon_sprite = BrowserPrefIconManager.GetSpriteFromName(String.Concat("Icons/", name));

                                //Set the sprite
                                buttonImage.GetComponent<Image>().sprite = icon_sprite;
                                buttonImage.sprite.name = name;
                            }

                        }
                    }
                }
            }
        }

        //Fortify Active company
        public void fortifyActiveCompany()
        {
            //Find the active company using the Company class state
            Company company = Company.SelectedCompany;
            String selectedCompanyName ="";
            TextMesh[] meshes = company.GetComponentsInChildren<TextMesh>();
            foreach(TextMesh mesh in meshes)
            {
                if (mesh.name.Equals("CompanyName"))
                {
                    selectedCompanyName = mesh.text.ToLower();
                }
            }
            // If not already fortiifed
            if (!companyNameFortifiedMap.ContainsKey(selectedCompanyName))
            {
                Debug.Log("Fortifying " + selectedCompanyName);
                //Update the threatScore for the company
                updateThreatScoreForACompany(selectedCompanyName);

                //Randomly toggle the permissions for a company
                randomlyTogglePermissionImagesFor(selectedCompanyName);

                //Fortify it!
                companyNameFortifiedMap[selectedCompanyName] = true;
            }
            else
            {
                Debug.Log("Already foritied: " + selectedCompanyName);
            }
        }

        //Calculate the positions of Foritfy button
        private void populateFortifyButtonPosition()
        {
            var XMin = 10000f;
            var XMax = -10000f;
            var YMax = -10000f;
            //Find the maxmimum X and Y coordinates of a Cube
            foreach (var cgoVect in companyGameObjOrgVectorMap)
            {
                XMin = Mathf.Min(XMin, cgoVect.Value.x);
                XMax = Mathf.Max(XMax, cgoVect.Value.x);
                YMax = Mathf.Max(YMax, cgoVect.Value.y);
            }
        }

        // Logic to physically construct the network edge game objects between the common tracked company nodes 
        private void constructNetworkEdges(Dictionary<CompanyData, List<TrackerInfo>> companyNetwork)
        {
            foreach (KeyValuePair<CompanyData, List<TrackerInfo>> compNet in companyNetwork)
            {
                CompanyData startCompData = compNet.Key;
                List<TrackerInfo> trackersList = compNet.Value;

                float x1 = startCompData.xpos * boxSeparation;
                float y1 = startCompData.ypos * boxSeparation;

                // Iterate over every tracker associated with the source company
                foreach (var tracker in trackersList)
                {
                    CompanyData endCompData = tracker.company;

                    // Null check to handle cases when the tracker company node doesnt exist
                    if (endCompData != null)
                    {
                        float x2 = endCompData.xpos * boxSeparation;
                        float y2 = endCompData.ypos * boxSeparation;

                        Vector3 midPoint = calcMidPoint(x1, y1, x2, y2);
                        float edgeYRotation = 0.0f;
                        float edgeZRotation;

                        // Handle Rotation
                        if (x1 == x2)
                        {
                            edgeZRotation = 0.0f;
                        }
                        else if (y1 == y2)
                        {
                            edgeZRotation = 90.0f;
                        }
                        else
                        {
                            edgeZRotation = getRotationAngle(x1, y1, x2, y2);
                            if (edgeZRotation >= 0)
                                edgeZRotation = 90.0f - edgeZRotation;
                        }

                        // Special case when net rotation about Z becomes negative, we need to compensate for it by
                        // spinning the edge about Y and then adding 90 to Z to make it positive 
                        if (edgeZRotation < 0)
                        {
                            edgeYRotation = 180.0f;
                            edgeZRotation += 90.0f;
                        }

                        // Get the euclidean distance between the points
                        float pointDist = calcDistance(x1, y1, x2, y2);

                        // Dynamically create a edge object
                        GameObject netEdge = Instantiate<GameObject>(NetworkEdgePrefab, CompanyLayoutManager);

                        // Position the object at the midpoint
                        netEdge.transform.localPosition = midPoint;

                        // Scale the edge to pierce the company cube on either sides
                        netEdge.transform.localScale = new Vector3(edgeRadius, edgeScaleFactor * pointDist, edgeRadius);

                        // Rotate the edge to align parallel to the line joining the two company cubes
                        netEdge.transform.rotation = Quaternion.Euler(0, edgeYRotation, edgeZRotation);

                        //Add to the list of edges
                        CompEdgeGameObjList.Add(netEdge);
                        
                        // Populate company game objects with the respective network game objects
                        compGameObjNetEdgeList[startCompData.name.ToLower()].Add(netEdge);
                        compGameObjNetEdgeList[endCompData.name.ToLower()].Add(netEdge);
                        
                        // Set initial state of edges
                        netEdge.SetActive(isGraphActive);
                    }
                }
            }
        }

        // Utility method to calculate distance between points
        private float calcDistance(float x1, float y1, float x2, float y2)
        {
            return Mathf.Sqrt(Mathf.Pow((x1 - x2), 2) + Mathf.Pow((y1 - y2), 2));
        }

        // Utility method to calculate the mid point between two company cubes
        private Vector3 calcMidPoint(float x1, float y1, float x2, float y2)
        {
            return new Vector3((x1 + x2) / 2, (y1 + y2) / 2, z_push);
        }

        // Utility method to find angle of rotation
        // flip boolean flag to reverse the angle of rotation
        private float getRotationAngle(float x1, float y1, float x2, float y2, bool flip = true)
        {
            if (flip)
            {
                return Mathf.Rad2Deg * Mathf.Atan((y2 - y1) / (x2 - x1)) * -1.0f;
            }
            return Mathf.Rad2Deg * Mathf.Atan((y2 - y1) / (x2 - x1));
        }

        // Utility method to build the adjacency list for the network of company objects
        private Dictionary<CompanyData, List<TrackerInfo>> populateNetworkAdjacencyList(List<CompanyData> companies)
        {
            Dictionary<CompanyData, List<TrackerInfo>> localCN = new Dictionary<CompanyData, List<TrackerInfo>>();
            foreach (CompanyData company in companies)
            {
                List<TrackerInfo> ti = new List<TrackerInfo>();
                foreach (Tracker tracker in company.trackers)
                {
                    foreach (string compName in tracker.companies)
                    {
                        ClusterLabelTrackerCount[company.category.ToLower()] += 1;
                        TrackerInfo trackerInfo = new TrackerInfo();

                        trackerInfo.data["name"] = tracker.name;
                        CompanyData cd;
                        if (companyNameObjMap.TryGetValue(compName.ToLower(), out cd))
                        {
                            trackerInfo.company = cd;
                        }

                        ti.Add(trackerInfo);
                    }
                }
                companyNetwork[company] = ti;
                localCN[company] = ti;
            }

            return localCN;
        }

        // Utility method to fetch json from file 
        private string fetchLocalJsonData()
        {
            return Resources.Load<TextAsset>("JSON/companyData").text;
        }

        // Utility method to fetch json from remote server
        private IEnumerator fetchRemoteJsonData(Boolean refreshed=false)
        {
            WWW www = new WWW(jsonDataUrl);
            yield return www;

            if (www.error == null)
            {
                if (refreshed)
                {
                    refreshReInitialize(www.text);
                }
                else
                {
                    InitializeData(www.text); 
                }
            }
            else
            {
                Debug.Log("Failed to retrieve data remotely | error: " + www.error);
                Debug.Log("Reverting to locally saved file!!");
                if (refreshed)
                {
                    refreshReInitialize(fetchLocalJsonData());
                }
                else
                {
                    InitializeData(fetchLocalJsonData());   
                }
            }
        }
        
        // Utility Method to give positions to the cubes
        private void PositionCompanyCubes(Dictionary<GameObject, Vector3> posDict, Boolean resetRotation=false)
        {
            foreach(KeyValuePair<GameObject, Vector3> entry in posDict)
            {
                entry.Key.transform.localPosition = entry.Value;
                if (resetRotation)
                {
                    entry.Key.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);   
                }
            }
        }

        // Utility to toggle network edges
        public void toggleEdges()
        {
            if (isLayoutReseted)
            {
                Debug.Log("ToggleEdges - current isActive: " + isGraphActive);
                if (CompEdgeGameObjList.Count == 0 && isSortingActive == false && isLabelOnly == false)
                {
                    isGraphActive = true;
                    
                    // Create edges in the company cubes network
                    constructNetworkEdges(companyNetwork);
                }
                //Dont allow toggle if sorted / in special layout
                else if(isGraphActive == false && isSortingActive==false && isLabelOnly == false)
                {
                    toggleNetworkEdges(true);
                    isGraphActive = true;
                }
                else
                {
                    toggleNetworkEdges(false);
                    isGraphActive = false;
                }   
            }
        }
        
        
        // Utility to toggle sorting of companies
        public void toggleSortingOfCompanies()
        {
            Debug.Log("ToggleSorting - current isActive: " + isSortingActive);
            if(isSortingActive && isLabelOnly == false && isLayoutReseted)
            {
                PositionCompanyCubes(companyGameObjOrgVectorMap);
                toggleClusterLabels(true);
                isSortingActive = false;
            }
            else if(isLabelOnly==false && isLayoutReseted)
            {
                sortCompanyCubes();
                toggleClusterLabels(false);
                isSortingActive = true;
            }
        }
        public void showOnlyLabelsToggle()
        {
            if (isLayoutReseted)
            {
                Debug.Log("showOnlyLabelsToggle - current isLabelOnly: " + isLabelOnly);
                //If currently only labels are shown then display the companies
                if (isLabelOnly)
                {
                    //show all companies
                    toggleCompanies(true);

                    //Position the cubes
                    PositionCompanyCubes(companyGameObjOrgVectorMap);

                    //unhide legend
                    threatScoreLegend.SetActive(true);

                    //Sort logic off, edges off, labelsonly off
                    isSortingActive = false;
                    isGraphActive = false;
                    isLabelOnly = false;
                
                    // Show Orbs on labels
                    toggleClusterLabelOrbs(false);
                }
                else
                {
                    //hide all companies
                    toggleCompanies(false);

                    //hide legend
                    threatScoreLegend.SetActive(false);

                    //hide graphs
                    toggleNetworkEdges(false);

                    //this is needed if going from sorted view to label only view
                    toggleClusterLabels(true);
                    isLabelOnly = true;
                
                    // Hide orbs on labels
                    toggleClusterLabelOrbs(true);
                }   
            }
        }

        // Utility Method to toggle cluster labels
        public void toggleClusterLabels(Boolean showLabel=true)
        {
            foreach (KeyValuePair<string, GameObject> entry in catNameLabelGameObjMap)
            {
                entry.Value.SetActive(showLabel);
            }
        }
        private void toggleCompanies(Boolean showLabel = true)
        {
            foreach (var company in companyNameGameObjMap)
            {
                companyNameGameObjMap[company.Key].SetActive(showLabel);
            }
        }

        private void toggleClusterLabelOrbs(Boolean showOrb = true)
        {
            foreach (var clo in ClusterLabelTransformMap)
            {
                clo.Value.SetActive(showOrb);
            }
        }
        

    // Utility Method to toggle network edges
        public void toggleNetworkEdges(Boolean showEdge)
            {
                foreach (GameObject netEdge in CompEdgeGameObjList)
                {
                    netEdge.SetActive(showEdge);
                }
            }
        
        // Utility method to sort the companies based off their threat scores
        public void sortCompanyCubes()
        {
            float[] threatScoreXPos = new[] {-4f, -2f, 0f, 2f, 4f};
            float[] threatScoreYPos = new[] {2f, 3f, 1f, 4f, 0f, 5f, -1f, 6f, -2f, 7f, -3f};

            int[] threatScoreFreq = new[] {0, 0, 0, 0, 0};
            
            Dictionary<GameObject, Vector3> sortedCompanies = new Dictionary<GameObject, Vector3>();

            foreach (KeyValuePair<CompanyData, GameObject> entry in companyObjGameObjMap)
            {
                CompanyData cd = entry.Key;
                int threatIndex = (int) (cd.threat_score / 20);
                Debug.Log(threatIndex + " " + cd.threat_score);
                
                sortedCompanies[entry.Value] = new Vector3(threatScoreXPos[threatIndex] * boxSeparation, 
                    threatScoreYPos[threatScoreFreq[threatIndex]] * boxSeparation, z_push);

                threatScoreFreq[threatIndex] += 1;
            }

            PositionCompanyCubes(sortedCompanies);
            toggleClusterLabels(false);
            toggleNetworkEdges(false);
        }

        public void highlightEdges(string compName)
        {
            if (isGraphActive && highlightEdgeOnFocus)
            {
                List<GameObject> netEdges = compGameObjNetEdgeList[compName];
                foreach (var ne in netEdges)
                {
                    ne.transform.Find("ConnectionCylinder").GetComponent<MeshRenderer>().material =
                        highlight_edge_material;
                }
            }
        }

        public void resetHighlightedEdges(string compName)
        {
            if (isGraphActive && highlightEdgeOnFocus)
            {
                List<GameObject> netEdges = compGameObjNetEdgeList[compName];
                foreach (var ne in netEdges)
                {
                    ne.transform.Find("ConnectionCylinder").GetComponent<MeshRenderer>().material =
                        default_edge_material;
                }
            }
        }
        
        public void highlightIndividualEdge(GameObject go)
        {
            if (isGraphActive && highlightEdgeOnFocus)
            {
                go.GetComponent<MeshRenderer>().material = individual_edge_material;
            }
        }

        public void resetIndividualHighlightedEdge(GameObject go)
        {
            if (isGraphActive && highlightEdgeOnFocus)
            {
                go.GetComponent<MeshRenderer>().material = default_edge_material;
            }
        }
        
        // Utility method invoked when some special layout is to be used
        public void setLayoutStyle(string layoutType)
        {
            isLayoutReseted = false;
        }
        
        // Utility method invoked to reset to initial plus layout
        public void resetLayoutStyle()
        {
            isLayoutReseted = true;
            toggleClusterLabels(true);
            if (isLabelOnly)
            {
                toggleClusterLabelOrbs(true);
            }
            else
            {
                toggleClusterLabelOrbs(false);
            }
            PositionCompanyCubes(companyGameObjOrgVectorMap, true);
            isGraphActive = false;
        }
    }

}