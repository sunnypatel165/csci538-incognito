﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HoloToolkit.MRDL.PeriodicTable
{
    public class Company : MonoBehaviour
    {
        private int PAST_SCANDAL_COUNT = 2;
        private int ALT_COUNT = 3;
        private int COOKIE_VALUE_COUNT = 3;
        
        public static Company SelectedCompany;

        public TextMesh ThreatScore;
        public TextMesh CompanyName;
        public GameObject Company_Logo;

        public GameObject[] CookieValues;

        public Text PastScandals_Content;
        public Button PastScandals_Btn;

        // Flame Prefabs
        public GameObject[] Flames;
        public Boolean ShowFlames = false;
        
        // Orb Prefabs
        public GameObject[] Orbs;
        public Boolean ShowOrbs = true;
        
//        Browser Permission Buttons        
        public Button cameraPerm_Btn;        
        public Button adsPerm_Btn;        
        public Button flashPerm_Btn;        
        public Button javascriptPerm_Btn;        
        public Button locationPerm_Btn;        
        public Button microphonePerm_Btn;        
        public Button notificationsPerm_Btn;        
        public Button paymentPerm_Btn;        
        public Button soundPerm_Btn;        
        public Button usbPerm_Btn;        
        
//        Alternative Companies Logos
        public Image[] AltComp_Logos;

        public Renderer BoxRenderer;
        public MeshRenderer[] PanelSides;
        public MeshRenderer PanelFront;
        public MeshRenderer PanelBack;
        public MeshRenderer[] InfoPanels;

        [HideInInspector]
        public CompanyData data;

        private BoxCollider boxCollider;
        private Material highlightMaterial;
        private Material dimMaterial;
        private Material clearMaterial;
        private PresentToPlayer present;
        public GameObject individualButton;
        public TextMesh hackThreatScore;

        public void SetSelectedCompany()
        {
            //individualButton.SetActive(false);
            foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("IndividualButton"))
            {
                fooObj.active = false;
            }
            Company company = gameObject.GetComponent<Company>();
            SelectedCompany = company;
            individualButton.SetActive(true);
            //get all children and set the button as active

        }

        public void ResetSelectedCompany()
        {
            SelectedCompany = null;
            individualButton.SetActive(false);
        }

        public void Start()
        {
            // Turn off our animator until it's needed
            GetComponent<Animator>().enabled = false;
            BoxRenderer.enabled = true;
            present = GetComponent<PresentToPlayer>();
        }

        public void Open()
        {
            if (present.Presenting)
                return;

            StartCoroutine(UpdateActive());
        }

        public void Highlight()
        {
            if (SelectedCompany == this)
                return;

            for (int i = 0; i < PanelSides.Length; i++)
            {
                PanelSides[i].sharedMaterial = highlightMaterial;
            }
            PanelBack.sharedMaterial = highlightMaterial;
            PanelFront.sharedMaterial = highlightMaterial;
            BoxRenderer.sharedMaterial = highlightMaterial;
        }

        public void Dim()
        {
            if (SelectedCompany == this)
                return;

            for (int i = 0; i < PanelSides.Length; i++)
            {
                PanelSides[i].sharedMaterial = dimMaterial;
            }
            PanelBack.sharedMaterial = dimMaterial;
            PanelFront.sharedMaterial = dimMaterial;
            BoxRenderer.sharedMaterial = dimMaterial;
        }

        public IEnumerator UpdateActive()
        {
            present.Present();

            while (!present.InPosition)
            {
                // Wait for the item to be in presentation distance before animating
                yield return null;
            }

            // Start the animation
            Animator animator = gameObject.GetComponent<Animator>();
            animator.enabled = true;
            animator.SetBool("Opened", true);

            while (Company.SelectedCompany == this)
            {
                yield return null;
            }

            animator.SetBool("Opened", false);

            yield return new WaitForSeconds(0.66f); // TODO get rid of magic number        

            present.Return();
            Dim();
        }
        
        public void SetFromCompanyData(CompanyData data, Dictionary<string, Material> typeMaterials)
        {
            this.data = data;
            
            // ************** Center Frame ************** 
            ThreatScore.text = data.threat_score.ToString();
            CompanyName.text = data.name;
            
            // Company Logo
            Texture2D logo_sprite = Resources.Load(data.logo_loc) as Texture2D;
            Company_Logo.GetComponent<RawImage>().texture = logo_sprite;
            
            // Threat score color
            ThreatScore.color = GetThreatScoreColor(data.threat_score, false);
            
            // Set Flame based off threat score
            if (ShowFlames)
            {
                Flames[(int)(data.threat_score / 20)].SetActive(true);
            }
            // Set Orbs based off threat score
            if (ShowOrbs)
            {
                Orbs[(int)(data.threat_score / 20)].SetActive(true);
            }
            //  **************  **************  ************** 

            // ************** Right Panel ************** 
            // Set Past Scandals Data
            SetPastScandals(data);

            // Build the cookie graph
            SetAndPopulateCookieGraph(data);
            //  **************  **************  ************** 

            // ************** Left Panel ************** 
            // Set Browser Permissions Data
            SetBrowserPerms(data);
            
            // Set Alternatives Data
            SetAlternatives(data);
            //  **************  **************  ************** 
            
            // Set up our materials
            if (!typeMaterials.TryGetValue(data.category.Trim(), out dimMaterial))
            {
                Debug.Log("Couldn't find " + data.category.Trim() + " in company " + data.name);
            }

            string highlightKey = data.category.Trim() + " highlight";
            if (!typeMaterials.TryGetValue(highlightKey, out highlightMaterial))
            {
                highlightMaterial = new Material(dimMaterial);
                highlightMaterial.color = highlightMaterial.color * 1.5f;
                typeMaterials.Add(highlightKey, highlightMaterial);
            }

            Dim();

            foreach (Renderer infoPanel in InfoPanels)
            {
                infoPanel.material.color = dimMaterial.color;
            }

            BoxRenderer.enabled = false;                     

            transform.parent.name = data.name;
        }

        private void SetBrowserPerms(CompanyData data)
        {
            for (int i = 0; i < data.browser_permissions.Length; i++)
            {
                Texture2D icon_texture = Resources.Load(data.browser_permissions[i].icon_loc) as Texture2D;
                Sprite icon_sprite = Sprite.Create(icon_texture, new Rect(0, 0, icon_texture.width,
                    icon_texture.height), new Vector2(0.5f, 0.5f));
                switch (data.browser_permissions[i].name.ToLower())
                {
                    case "camera":
                        cameraPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        cameraPerm_Btn.GetComponent<Image>().sprite.name = data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "ads":
                        adsPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        adsPerm_Btn.GetComponent<Image>().sprite.name = data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "flash":
                        flashPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        flashPerm_Btn.GetComponent<Image>().sprite.name = data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "javascript":
                        javascriptPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        javascriptPerm_Btn.GetComponent<Image>().sprite.name =
                            data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "location":
                        locationPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        locationPerm_Btn.GetComponent<Image>().sprite.name = data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "microphone":
                        microphonePerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        microphonePerm_Btn.GetComponent<Image>().sprite.name =
                            data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "notifications":
                        notificationsPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        notificationsPerm_Btn.GetComponent<Image>().sprite.name =
                            data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "payment":
                        paymentPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        paymentPerm_Btn.GetComponent<Image>().sprite.name = data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "sound":
                        soundPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        soundPerm_Btn.GetComponent<Image>().sprite.name = data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                    case "usb":
                        usbPerm_Btn.GetComponent<Image>().sprite = icon_sprite;
                        usbPerm_Btn.GetComponent<Image>().sprite.name = data.browser_permissions[i].icon_loc.Split('/')[1];
                        break;
                }
            }
        }

        private void SetAlternatives(CompanyData data)
        {
            for (int i = 0; i < ALT_COUNT; i++)
            {
                Texture2D alt_logo_texture = Resources.Load(data.alternatives[i].logo_loc) as Texture2D;
                Sprite alt_logo_sprite = Sprite.Create(alt_logo_texture, new Rect(0, 0, alt_logo_texture.width,
                    alt_logo_texture.height), new Vector2(0.5f, 0.5f));

                AltComp_Logos[i].sprite = alt_logo_sprite;
            }
        }

        private void SetPastScandals(CompanyData data)
        {
            string past_scandals_data = "";
            for (int i = 0; i < Math.Min(PAST_SCANDAL_COUNT, data.past_scandals.Length); i++)
            {
                past_scandals_data = string.Concat(past_scandals_data, "• ", data.past_scandals[i].headline);
                past_scandals_data = string.Concat(past_scandals_data, "\n");
            }

            PastScandals_Content.text = past_scandals_data;
            PastScandals_Btn.onClick.AddListener(() => Application.OpenURL(data.past_scandals[0].source));
        }

        private void SetAndPopulateCookieGraph(CompanyData data)
        {
            int userCookiesCount = data.cookie_statistics.user_cookies;
            int sessionCookiesCount = data.cookie_statistics.session_cookies;
            int thirdPartyCookiesCount = data.cookie_statistics.thirdparty_cookies;

            CookieValues[0].transform.localScale -= new Vector3(0, userCookiesCount, 0); //scale down the bar 
            CookieValues[0].transform.localPosition -=
                new Vector3(0, userCookiesCount / 2, 0); //adjust position since the size decreases from both sides
            CookieValues[1].transform.localScale -= new Vector3(0, sessionCookiesCount, 0);
            CookieValues[1].transform.localPosition -= new Vector3(0, sessionCookiesCount / 2, 0);
            CookieValues[2].transform.localScale -= new Vector3(0, thirdPartyCookiesCount, 0);
            CookieValues[2].transform.localPosition -= new Vector3(0, thirdPartyCookiesCount / 2, 0);
        }

        public static Color GetThreatScoreColor(float score, bool alwaysWhite = false)
        {
            if (alwaysWhite)
            {
                return Color.white;
            }
            Color color = Color.white;
            if (score <= 20.0)
            {
                ColorUtility.TryParseHtmlString("#37B4F2", out color);
            }
            else if (score > 20.0 && score < 40.0)
            {
                ColorUtility.TryParseHtmlString("#37F248", out color);
            }
            else if (score >= 40.0 && score < 60.0)
            {
                ColorUtility.TryParseHtmlString("#F2F237", out color);
            }
            else if (score >= 60.0 && score < 80.0)
            {
                ColorUtility.TryParseHtmlString("#F29B37", out color);
            }
            else if (score >= 80.0)
            {
                ColorUtility.TryParseHtmlString("#F23E37", out color);
            }
            return color;
        }


    }
}
