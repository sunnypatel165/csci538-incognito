This project is created for CSCI 538: Augmented, Virtual and Mixed Reality class at USC.

**Breif goals of this project:**
- Visualise user specific browsing data
- Visualise connections between websites, provided by Symantec's cookie analysis tools
- Give users an update on how his cookie data could be used and show past breaches
- Allow users to fortify themselves 
- Visualise large amounts of data in 3D and use hololens capabiities to make use of space 

**More information - trailer, website and documents can be found [here](https://drive.google.com/drive/folders/1cXEHzVdIKte2plP5un56S1PcDys0X3f0?usp=sharing)**

Unity version used during development: 2019.1.14f1

Feel free to contact sunnypatel165@gmail.com or sarveshsparab@gmail.com for walkthrough and/or ideas!

**Licensing and contributing:**

- This project was made with the Hololens MRDL starter projects. 
- The original project can be found [here](https://github.com/microsoft/MRDL_Unity_PeriodicTable)
- The project is purely educational and no profit is inteded from this. 
- Any further copies of this project should not intend to make money out of it without Microsoft's consent.
- Please read the licensing at MRDL and Microsoft websites before extending this.